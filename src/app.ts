import cookieParser from 'cookie-parser'
import express, {Express} from 'express'
import {router as fileBasedRouter} from 'express-file-routing'
import {prisma} from './globals'

async function main() {

	const app: Express = express()

	app.use(cookieParser())
	app.use(express.urlencoded({extended: true}))

	app.use(express.static('public'))

	app.use('/', await fileBasedRouter())

	//static routing should be the absolute last option for any request.
	const PORT = 8080

	// Console usage is important here.
	// eslint-disable-next-line no-console
	app.listen(PORT, () => { console.log('server started at localhost:8080') })
}

main().then(async () => {
	await prisma.$disconnect()
}).catch(async (e) => {
	console.error(e)
	await prisma.$disconnect()
	process.exit(1)
})
