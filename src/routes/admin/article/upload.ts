import {Request, Response} from 'express'
import articleUpload from '../../../components/articleUpload'
import root from '../../../components/root'
import authenticate from '../../../middleware/authenticate'

export const get = [authenticate(), (req: Request, res:Response) => {
	res.send(root(articleUpload, req))
}]
