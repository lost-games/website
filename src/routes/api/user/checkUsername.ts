import {Request, Response} from 'express'
import {prisma} from '../../../app'

export const get = async(req:Request, res:Response) => {
	try {
		const username:string = req.query.username?.toString() ?? ''
		await prisma.user.findFirstOrThrow({where: {name: username}})
		res.send(/*html*/`<p class="usernameAvailability">Username ${username} is already taken.</p>`)
	} catch (e) {
		res.send('<p class="usernameAvailability">Available!</p>')
	}
}
