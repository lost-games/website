import {Request, Response} from 'express'
import articlePreview from '../../components/articlePreview'
import {INTERNAL_SERVER_ERROR} from '../../data/constants'
import numeric from '../../middleware/numeric'

export const get = [numeric('skip'),
	async (req:Request, res:Response) =>{
		try {
			res.send(await articlePreview(parseInt(req.params.skip)))
		} catch (e) {
			res.sendStatus(INTERNAL_SERVER_ERROR)
		}
	}]
