import {Request, Response} from 'express'
import root from '../components/root'

export const get = async (req: Request, res: Response) => {
	res.send(await root('/html/index.html', req))
}
