import {Request, Response} from 'express'

export const get = (req:Request, res:Response) => {
	if (req.cookies.authJWT !== undefined) {
		res.clearCookie('authJWT')
		res.clearCookie('xsrf')
	}
	res.setHeader('HX-Redirect', '/')
	res.send()
}
