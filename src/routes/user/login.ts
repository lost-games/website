import {Request, Response} from 'express'
import loginWidget from '../../components/loginWidget'
import root from '../../components/root'

export const get = async (req:Request, res:Response) => {
	res.send(await root(loginWidget(req), req))
}
