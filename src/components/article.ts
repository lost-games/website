import Showdown from 'showdown'
import {articleCached} from '../handlers/cache'

export async function article(_id:number):Promise<string> {
	return /*html*/`
    <article>
        ${new Showdown.Converter()
		.makeHtml((await articleCached(_id)).content)}
    </article>`
}
