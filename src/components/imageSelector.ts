export default () => {
	return /*html*/`
		<div class="imageSelector">
			<input type="text" id="searchBox"></input>
			<input type="button" hx-on:click="htmx.trigger('#imageList', 'lg-search', {})"></button>
			<ul id="imageList" hx-get="/api/data/images" hx-include="#searchBox" hx-trigger="load,lg-search"></ul>
		</div>
	`
}
