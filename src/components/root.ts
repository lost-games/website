import {Request} from 'express'
import {header} from './header'

type funcWithArg = {
	func: ((n:number)=>string),
	val: number
}

export default async function root(content:string | (() => string) | funcWithArg, request:Request): Promise<string> {
	const handler = async (content:string, request:Request) => {
		return /*html*/`
		<!DOCTYPE html>
		<html>
		<head>
		<script src="https://unpkg.com/htmx.org@1.9.5"></script>
		<link rel="stylesheet" type="text/css" href="/css/site.css"/>
		</head>
		<body>
			${await header(request)}
			<div class="container">
				<div class="sidebar"></div>
					<div class="content">
						${content}
					</div>
				<div class="sidebar"></div>
			</div>
		</body>
	</html>`
	}

	const rxp = /^\/\w+(?:\/\w+)*\/?(?:\.html)?$/

	if (typeof content === 'function') {
		return handler(content(), request)
	}
	if (typeof content !== 'string') {
		return handler(content.func(content.val), request)
	}
	if (content.match(rxp) !== null) {
		return handler(`<div hx-get="${content}" hx-swap="outerHTML" hx-trigger="load"> </div>`, request)
	}
	return handler(content, request)
}
