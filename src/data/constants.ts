export const JWTEXPIRATION = 2160
export const OK = 200
export const REDIRECT_FOUND = 302
export const INVALID_REQUEST = 400
export const NOT_FOUND = 404
export const FORBIDDEN = 403
export const INTERNAL_SERVER_ERROR = 500
export const POST_BATCH_SIZE = 4
