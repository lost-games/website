import {NextFunction, Request, Response} from 'express'
import {verify} from 'jsonwebtoken'
import {FORBIDDEN} from '../data/constants'
import {prisma} from '../globals'

export default () => async (req:Request, res:Response, next:NextFunction) => {
	if (req.cookies.authJWT === undefined || req.cookies.xsrf === undefined) {
		res.status(FORBIDDEN)
		res.redirect('/')
	} else {
		const token = req.cookies.authJWT
		const xsrf = req.cookies.xsrf
		try {
			const validatedToken = verify(token, process.env.JWT_SECRET ?? '')
			if (typeof validatedToken === 'string' || validatedToken.XSRF === undefined || validatedToken.username === undefined) {
				res.status(FORBIDDEN)
				res.redirect('/')
			} else if (xsrf === validatedToken.XSRF) {
				await prisma.user.findUniqueOrThrow({where: {name: validatedToken.username}})
				next()
			}
		} catch (e) {
			res.status(FORBIDDEN)
			res.redirect('/')
		}
	}
}
