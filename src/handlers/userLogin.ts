import bcrypt from 'bcrypt'
import {Request} from 'express'
import {prisma} from '../globals'

/**
 * Validates a login request from express.
 * @param req the request object to be validated.
 */
export default async (req:Request):Promise<boolean>  => {
	if (req.body === null) { return false }
	const userInputName:string = req.body.name
	const inputPassword = req.body.password
	const user = await prisma.user.findFirstOrThrow({
		where: {name: userInputName},
	})
	if (user === undefined || user === null) {
		return false
	}
	return bcrypt.compare(inputPassword, user.passwordHash)
}
