import {Request, Response} from 'express'
import {REDIRECT_FOUND} from '../data/constants'

export default async (res:Response, target:string | Request, useHtmx:boolean = true):Promise<void> => {
	let targetURI:string
	if (typeof target === 'string') {
		targetURI = target
	} else {
		targetURI = target.params.returnURI ?? '/'
	}
	if (useHtmx) {
		res.setHeader('hx-redirect', targetURI)
		res.sendStatus(REDIRECT_FOUND)
	} else {
		res.redirect(targetURI)
		res.sendStatus(REDIRECT_FOUND)
	}
}
